This repository contains code samples from projects I've worked on.  Currently, I have my Data Science and Visualization project up and my recent Analysis of Algorithms project up. 

In my Data Science directory, Network.py is the python script that inputs a csv and creates a network based on the 5 nearest neighbors of a data point (and sizes these nodes based on their PageRank) and Lab6.html is the HTML/JavaScript/D3.js file with the accompanying visualization.  

My Analysis of Algorithm project has various files that were included in the creation of the main algorithm and side algorithms that expanded upon the project.  The most important of these is ProjectCode340.py because this holds the code for the core algorithm of the project.