#some fun imports probably will go here



# to implement the linked list we must first have a node...

class Node(object):

	def __init__(self, data=None, nextnode=None):
		self.data = data
		self.nextnode = nextnode

	def getdata(self):
		return self.data

	def getnext(self):
		return self.nextnode

	def assignnext(self, newnext):
		self.nextnode = newnext

#the first node in the list -- useful for iterating-- the head node! now it looks like we will have a linked list !!! ~hizaa~


class linkedlist(object):
	def __init__(self, head= None,size=None):
		self.head = head
		self.size = 0 #if we store the size, incrementing everytime we add one node, we never have to traverse it to find the size-- requiring no additional time!!!!

## for our beautiful linked list we will most likely only need three functions: Insert, delete, and get the size ( maybe not this one but why not)

	def insert(self, data):
		newnode = Node(data)
		newnode.assignnext(self.head)
		self.head = newnode
		self.size += 1



# do we even need to delete the head? maybe not?..... probably.....
	def deletehead(self):
		current_node = self.head
		self.head = current_node.nextnode
		self.size = self.size - 1

	def linkedlist2string(self):
		string = ''
		test = self
		while test.size > 0:
			#print ('maddy i love u',test.head.getdata())
			string += str(test.head.getdata()) + " "
			self.deletehead()
		#print ('i hate',string)
		return string


"""
heres some basic operations of the linkedlist class

>> lst = linkedlist() - creates empty linked list
>> lst.size - inits size at zero
0
>> lst.insert(3)
>> lst.size   - now size is one !
1
>> lst.head      - just points to the place in memory where head is located
<__main__.Node object at 0x105073510>
>> lst.head.getdata()    -the node at 0x105073510
3
>> lst.insert(4)
>> lst.head.getdata() - new head is assigned!
4
>> lst        - just points to a place in memory
<__main__.linkedlist object at 0x1050664d0>
>> lst.size
2
>> lst.head.getnext()  - getnext() operation just points to a nodes' place in memory
<__main__.Node object at 0x105073510>
>> lst.head.getnext().getdata() - the node located at 0x105073510
3
"""
