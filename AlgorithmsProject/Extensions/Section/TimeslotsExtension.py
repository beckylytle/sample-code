import Queue
from linkedlist import *
from SetUp import *

def CreateSchedule(rooms, timeslot,ClassProf,ClassConflict,ClassEnrollInfo,ClassRoom,SortedRoom,StudentsClasses,StudentsTimeSlot,ClassPriority,Schedule,ProfessorConflicts):
    timeroom = {}
    for m in range(0,len(timeslot)):
        for k in range(0,len(SortedRoom)):
            timeroom[(m,k)] = False
    k=0 #the number of columns (aka which classroom to use)
    m=0 #the number of rows (aka which timeslot to use)
    countdown = False #this lets us know what to do with m
    n = len(ClassPriority) #n is the length of classes
    i = 0 #this lets us loop through ClassPriority
    ClassTimeSlot = {}
    for e in range(0,len(ClassPriority)):
        ClassTimeSlot[ClassPriority[e]] = None
        """NEW DATA STRUCTURE"""
    #let ClassProf[i] be the professor teaching a certain class i
    ClassStudents = {}
    for q in range(0,len(ClassPriority)):
        ClassStudents[ClassPriority[q]] = linkedlist()
        #set all values to empty lists to assign students at the end
    while i < n:
        #print (i,m,k,countdown)
        #print ('Current Class: ',ClassPriority[i])
        #print ('Current m:',m)
        #print ('Current k:',k)
        if ClassRoom[ClassPriority[i]] == None: #if class hasn't already been assigned
            if ProfessorConflicts[ClassProf[ClassPriority[i]]] == None: #Aka if professor has no previous assignments
                ProfessorConflicts[ClassProf[ClassPriority[i]]] = [timeslot[m]] #Set professor assignment to class i
                Schedule[timeslot[m]].insert(ClassPriority[i]) #add this class to the schedule at this time
                if m%2 == True:
                    ClassRoom[ClassPriority[i]] = SortedRoom[k+1]
                else:
                    ClassRoom[ClassPriority[i]] = SortedRoom[k]
                ClassTimeSlot[ClassPriority[i]] = timeslot[m] #ADD THIS TIME SLOT TO THIS CLASS
                timeslotFill = True
                timeroom[(m,k)] = True
            elif (timeslot[m] in ProfessorConflicts[ClassProf[ClassPriority[i]]]): #Aka if professor is already in this time slot!
                timeslotFill = False
                ClassConflict.put(ClassPriority[i])
            elif m > 0 and (timeslot[m-1] in ProfessorConflicts[ClassProf[ClassPriority[i]]]):
                timeslotFill = False
                ClassConflict.put(ClassPriority[i])
            elif m < len(timeslot)-1 and (timeslot[m+1] in ProfessorConflicts[ClassProf[ClassPriority[i]]]):
                timeslotFill = False
                ClassConflict.put(ClassPriority[i])
            else:
                Schedule[timeslot[m]].insert(ClassPriority[i]) #add this class to the schedule at this time
                if m%2 == True:
                    ClassRoom[ClassPriority[i]] = SortedRoom[k+1]
                else:
                    ClassRoom[ClassPriority[i]] = SortedRoom[k]
                ClassTimeSlot[ClassPriority[i]] = timeslot[m] #ADD THIS TIME SLOT TO THIS CLASS
                ProfessorConflicts[ClassProf[ClassPriority[i]]] += [timeslot[m]]
                timeslotFill = True
                timeroom[(m,k)] = True
            j = i
            """Run the following loop until current time slot is filled"""
            while timeslotFill == False and j+1<len(ClassPriority): #Run this loop until this specific time slot is filled!!!
            #don't continue this loop if you're at the end of the class list.. Just let this timeslot be empty
                if not ProfessorConflicts[ClassProf[ClassPriority[j+1]]] == None and timeslot[m] in ProfessorConflicts[ClassProf[ClassPriority[j+1]]]: #check if class j+1 works
                    ClassConflict.put(ClassPriority[j+1]) #put class j+1 on queue
                    j=j+1 #increment j so we can try next class
                elif not ProfessorConflicts[ClassProf[ClassPriority[j+1]]] == None and m<len(timeslot)-1 and timeslot[m+1] in ProfessorConflicts[ClassProf[ClassPriority[j+1]]]:
                    ClassConflict.put(ClassPriority[j+1]) #put class j+1 on queue
                    j=j+1 #increment j so we can try next class
                elif not ProfessorConflicts[ClassProf[ClassPriority[j+1]]] == None and m>0 and timeslot[m-1] in ProfessorConflicts[ClassProf[ClassPriority[j+1]]]:
                    ClassConflict.put(ClassPriority[j+1]) #put class j+1 on queue
                    j=j+1 #increment j so we can try next class
                else:
                    #print ("Got here for",ClassPriority[j+1])
                    #print ('Filling timeslot ',timeslot[m],' with ',ClassPriority[j+1])
                    Schedule[timeslot[m]].insert(ClassPriority[j+1]) #add this class to schedule at this time
                    if ProfessorConflicts[ClassProf[ClassPriority[j+1]]] == None:
                        ProfessorConflicts[ClassProf[ClassPriority[j+1]]] = [timeslot[m]] #assign professor to this timeslot
                    else:
                        ProfessorConflicts[ClassProf[ClassPriority[j+1]]] += [timeslot[m]]
                        #assign room k to this class
                    if m%2 == True:
                        ClassRoom[ClassPriority[i]] = SortedRoom[k+1]
                    else:
                        ClassRoom[ClassPriority[i]] = SortedRoom[k]
                    ClassTimeSlot[ClassPriority[j+1]] = timeslot[m] #ADD THIS TIME SLOT TO THIS CLASS
                    timeslotFill = True
                    timeroom[(m,k)] = True
            #print Schedule
            """Now, run through the Queue and place these Classes in time slots"""
            if ClassConflict.empty() == False:
                while ClassConflict.empty() == False:
                    #print ('m',m)
                    #print ('k',k)
                    #while queue is not empty
                    #PROBLEM: this enters infinite loop if current time slot works for nothing on the queue!
                    #HOW TO FIX: check if current time slot has already been tried by everything on queue!
                    q = ClassConflict.get() #q is the class popped off the queue
                    #print q
                    origM = m
                    origK = k
                    origCD = countdown #save all current values
                    #print m
                    if m==len(timeslot)-1 and countdown == False: #aka if m is the last timeslot in the list of timeslots
                        countdown = True #let countdown be True
                        k=k+2
                        m=m+1 #increment both k and m
                    elif m==0 and countdown == True: #aka if m is at the first time slot in the list of timeslots & it's been counting down
                        countdown = False #switch value of countdown
                        k=k+2
                        m=m-1 #increment k, subtract 1 from m
                    if countdown == True: #if we're counting down rn
                        #print m
                        m=m-1
                        #print 'decrement m'
                        #print m
                    else:
                        m=m+1
                        #print 'increment m'
                    """ The above stuff is just a way to increment the time slot and classroom in the way we discussed
                    This is necessary because the original timeslot did not work for these & that's why they're on the Queue"""
                    if (timeslot[m] in ProfessorConflicts[ClassProf[q]]):
                        if ClassConflict.empty() == False:
                            m = origM
                            k = origK
                            Countdown = origCD #Reset all these values
                            ClassConflict.put(q) #add q to ClassConflict again
                        else:
                            ClassConflict.put(q)
                            #print m
                        #now, we'll try the next item in the queue on this
                    elif m>0 and timeslot[m-1] in ProfessorConflicts[ClassProf[q]]:
                        if ClassConflict.empty() == False:
                            m = origM
                            k = origK
                            Countdown = origCD #Reset all these values
                            ClassConflict.put(q) #add q to ClassConflict again
                        else:
                            ClassConflict.put(q)
                            #print m
                        #now, we'll try the next item in the queue on this
                    elif m<len(timeslot)-1 and timeslot[m+1] in ProfessorConflicts[ClassProf[q]]:
                        if ClassConflict.empty() == False:
                            m = origM
                            k = origK
                            Countdown = origCD #Reset all these values
                            ClassConflict.put(q) #add q to ClassConflict again
                        else:
                            ClassConflict.put(q)
                            #print m
                        #now, we'll try the next item in the queue on this
                    else: #if this timeslot works for
                        Schedule[timeslot[m]].insert(q) #add this class to schedule at this time
                        if ProfessorConflicts[ClassProf[q]] == None:
                            ProfessorConflicts[ClassProf[q]] = [timeslot[m]] #assign professor to this timeslot
                        else:
                            ProfessorConflicts[ClassProf[q]] += [timeslot[m]]
                        if m%2 == True:
                            ClassRoom[ClassPriority[i]] = SortedRoom[k+1]
                        else:
                            ClassRoom[ClassPriority[i]] = SortedRoom[k]
                        ClassTimeSlot[q] = timeslot[m] #ADD THIS TIME SLOT TO THIS CLASS
                        timeroom[(m,k)] = True
                        #print ('Putting class ',q," in timeslot ",timeslot[m])
                        #print ('Assigning',q,"from queue")
            elif ClassConflict.empty() == True:
                if m==len(timeslot)-1 and countdown == False: #aka if m is the last timeslot in the list of timeslots
                    countdown = True #let countdown be True
                    k=k+2
                    m=m+1 #increment both k and m
                    #print "CHECKPOINT1"
                elif m==0 and countdown == True: #aka if m is at the first time slot in the list of timeslots & it's been counting down
                    countdown = False #switch value of countdown
                    k=k+2
                    m=m-1
                    #increment k, subtract 1 from m
                    #print "CHECKPOINT2"
                if countdown == True: #if we're counting down rn
                    m=m-1
                    #print "CHECKPOINT3"
                else:
                    m=m+1
                    #print "CHECKPOINT4"
                """Again, this is just incrementing the time slot to the next one that makes sense based on
                how we wanted to assign time slots"""
        else:
            if m==len(timeslot)-1 and countdown == False: #aka if m is the last timeslot in the list of timeslots
                countdown = True #let countdown be True
                k=k+2
                m=m+1 #increment both k and m
                #print "CHECKPOINT1"
            elif m==0 and countdown == True: #aka if m is at the first time slot in the list of timeslots & it's been counting down
                countdown = False #switch value of countdown
                k=k+2
                m=m-1
                #increment k, subtract 1 from m
                #print "CHECKPOINT2"
            if countdown == True: #if we're counting down rn
                m=m-1
                #print "CHECKPOINT3"
            else:
                m=m+1
                #print "CHECKPOINT4"
        i = i + 1 #increment i
    #print ('StudentsTimeSlot',StudentsTimeSlot)
    #For each class j in ClassPriority (starting with least popular class):  """!!!!!!!!"""
    experimentalCounter = 0 #THIS WILL COUNT the amount of preferences HONORED!!
    z = len(ClassPriority) - 1 #Start with last element & go down
    while z > -1:
        #print ('Current Class',ClassPriority[z])
        Size = rooms[ClassRoom[ClassPriority[z]]] #get size of classroom that this class is in!
        #Size = 50
        #print Size
        a=0 #a represents going through size of classroom
        b=0 #b represents the specific student
        OriginalHead = ClassEnrollInfo[ClassPriority[z]].head
        #print ('ADDING STUDENTS TO CLASS', ClassPriority[z])
        #print ('SIZE OF CLASS',Size)
        #print ('STUDENTS THAT WANT THIS CLASS',ClassEnrollInfo[ClassPriority[z]].size)
        while a <= Size and not OriginalHead == None: #running through possible students in class
                # StudentsClasses[student] holds class pref list and final enrollment
            #print ('a',a,'b',b)

            #currentstudent = ClassEnrollInfo[ClassPriority[z]][1][b] the bth student in list of students who want this class
            currentstudent = ClassEnrollInfo[ClassPriority[z]].head
            #print ('CURRENT STUDENT:',currentstudent)
            currenttimeslot = ClassTimeSlot[ClassPriority[z]] #timeslot of class z
            #print ('timeslot',currenttimeslot)
            #print ('student',currentstudent.data)
            #print StudentsTimeSlot
            #print ('???',StudentsTimeSlot[currentstudent.data][currenttimeslot-1])
            if currenttimeslot == 1:
                if StudentsTimeSlot[currentstudent.data][currenttimeslot-1] == None and StudentsTimeSlot[currentstudent.data][currenttimeslot] == None: #if student doesn't have a conflict at time of class z
                    StudentsClasses[currentstudent.data][1].insert([ClassPriority[z]])
                    experimentalCounter += 1
                    #StudentsClasses[currentstudent.data][1] += [ClassPriority[z]] #add class z to enrollment list of student b
                    StudentsTimeSlot[currentstudent.data][currenttimeslot-1] = ClassPriority[z] #assign class to student's time slot
                    StudentsTimeSlot[currentstudent.data][currenttimeslot] = ClassPriority[z]
                    ClassStudents[ClassPriority[z]].insert(currentstudent.data)
                    #print 'Added current student to ClassStudents'
                    """ADD TO CLASS SCHEDULE"""
                    a = a+1
                    #increment a so we know a seat in the room has been taken
            elif currenttimeslot >= len(timeslot)-1:
                if StudentsTimeSlot[currentstudent.data][currenttimeslot-1] == None and StudentsTimeSlot[currentstudent.data][currenttimeslot-2] == None: #if student doesn't have a conflict at time of class z
                    StudentsClasses[currentstudent.data][1].insert([ClassPriority[z]])
                    experimentalCounter += 1
                    #StudentsClasses[currentstudent.data][1] += [ClassPriority[z]] #add class z to enrollment list of student b
                    StudentsTimeSlot[currentstudent.data][currenttimeslot-1] = ClassPriority[z]
                    StudentsTimeSlot[currentstudent.data][currenttimeslot-2] = ClassPriority[z] #assign class to student's time slot
                    ClassStudents[ClassPriority[z]].insert(currentstudent.data)
                    #print 'Added current student to ClassStudents'
                    """ADD TO CLASS SCHEDULE"""
                    a = a+1
                    #increment a so we know a seat in the room has been taken
            else:
                if StudentsTimeSlot[currentstudent.data][currenttimeslot-1] == None and StudentsTimeSlot[currentstudent.data][currenttimeslot-2] == None and StudentsTimeSlot[currentstudent.data][currenttimeslot] == None: #if student doesn't have a conflict at time of class z
                    StudentsClasses[currentstudent.data][1].insert([ClassPriority[z]])
                    experimentalCounter += 1
                    #StudentsClasses[currentstudent.data][1] += [ClassPriority[z]] #add class z to enrollment list of student b
                    StudentsTimeSlot[currentstudent.data][currenttimeslot] = ClassPriority[z]
                    StudentsTimeSlot[currentstudent.data][currenttimeslot-2] = ClassPriority[z]
                    StudentsTimeSlot[currentstudent.data][currenttimeslot-1] = ClassPriority[z] #assign class to student's time slot
                    ClassStudents[ClassPriority[z]].insert(currentstudent.data)
                    #print 'Added current student to ClassStudents'
                    """ADD TO CLASS SCHEDULE"""
                    a = a+1
                    #increment a so we know a seat in the room has been taken
            b=b+1 #move through student list no matter what
            ClassEnrollInfo[ClassPriority[z]].head = currentstudent.nextnode #Set new head to current head's next node!
            #if b is now bigger than the length of students who wanted this class, exit loop!!
            #if b >= len(ClassEnrollInfo[ClassPriority[z]][1]):
            if b >= ClassEnrollInfo[ClassPriority[z]].size:
                a = Size + 1 #exit loop by breaking loop condition
            elif currentstudent.nextnode == None:
                a = Size + 1
        ClassEnrollInfo[ClassPriority[z]].head = OriginalHead #set head back to original
        z = z - 1
    #return (StudentsClasses,Schedule)
    #print ClassStudents
    #print StudentsClasses
    print ('HONORED',experimentalCounter)
    print ('TOTAL',4*len(StudentsClasses.keys()))

    return (ClassRoom,ClassProf,ClassTimeSlot,ClassStudents) #these dictionaries have all info
    """ this return statement will b replaced w writing to a txt file!!"""


"""THIS NEXT PART IS OUTSIDE THE FUNCTION!!
it's all sample data right now; in future, we will have to intake txt files and create the following
data structures. also, i don't know how to make a linked list (single or double) so currently i'm using
lists in place of this bc i'm on a plane & have no reliable internet.

Also, the following are things that r actually passed into the function and used.

All the Sample Data that these came from are in 340projectcodeSetUp.py"""


class1info = linkedlist()
class1info.insert(10)
class1info.insert(9)
class1info.insert(8)
class1info.insert(7)
class1info.insert(6)
class1info.insert(5)
class1info.insert(4)
class1info.insert(3)
class1info.insert(2)
class1info.insert(1)

class2info = linkedlist()
class2info.insert(10)
class2info.insert(9)
class2info.insert(8)
class2info.insert(6)
class2info.insert(5)
class2info.insert(4)
class2info.insert(1)


class3info = linkedlist()
class3info.insert(8)
class3info.insert(7)
class3info.insert(6)
class3info.insert(2)
class3info.insert(1)

class4info = linkedlist()
class4info.insert(10)
class4info.insert(8)
class4info.insert(6)
class4info.insert(3)
class4info.insert(2)
class4info.insert(1)

class5info = linkedlist()
class5info.insert(10)
class5info.insert(9)
class5info.insert(7)
class5info.insert(5)
class5info.insert(4)
class5info.insert(3)
class5info.insert(2)

class6info = linkedlist()
class6info.insert(9)
class6info.insert(7)
class6info.insert(5)
class6info.insert(4)
class6info.insert(3)


ClassEnrollInfo = {
1: class1info,
2: class2info,
3: class3info,
4: class4info,
5: class5info,
6: class6info
}
"""
ClassEnrollInfo = {
1: [10, [1,2,3,4,5,6,7,8,9,10]],
2: [7, [1,4,5,6,8,9,10]],
3: [5, [1,2,6,7,8]],
4: [6, [1,2,3,6,8,10]],
5: [7, [2,3,4,5,7,9,10]],
6: [5, [3,4,5,7,9]]
}
"""
ClassRoom = {1:None, 2:None, 3:None, 4:None, 5:None, 6:None}

SortedRoom = [1,2,3]

"""I set their lists to empty lists instead of lists w None in them"""
StudentsClasses = {
1:[[1,2,3,4],linkedlist()],
2:[[1,3,4,5],linkedlist()],
3:[[1,4,5,6],linkedlist()],
4:[[1,5,6,2],linkedlist()],
5:[[1,6,2,5],linkedlist()],
6:[[1,3,4,2],linkedlist()],
7:[[1,6,5,3],linkedlist()],
8:[[1,2,3,4],linkedlist()],
9:[[1,5,6,2],linkedlist()],
10:[[1,2,4,5],linkedlist()]
}


StudentsTimeSlot = {
1:[None,None], #2 time slots possible
2:[None,None],
3:[None,None],
4:[None,None],
5:[None,None],
6:[None,None],
7:[None,None],
8:[None,None],
9:[None,None],
10:[None,None]
}

ClassPriority = [
1,2,5,4,3,6 #most to least popular
]

Schedule = {
1: linkedlist(),
2: linkedlist()
}

ProfessorConflicts = {
1:None,
2:None,
3:None
}

ClassConflict = Queue.Queue() #creates empty queue

ClassProf = {1:1,2:1,3:2,4:2,5:3,6:3}
"""THIS IS NEW, shows which class is w/ what professor"""
#THIS IS STATIC !! might as well be given to us

"""THESE WERE GIVEN TO US!!!! essentially"""
timeslot = [1,2]
rooms =  {1:10,2:7,3:5}

#Call FUNCTION
#print CreateSchedule(rooms,timeslot,ClassProf,ClassConflict,ClassEnrollInfo,ClassRoom,SortedRoom,StudentsClasses,StudentsTimeSlot,ClassPriority,Schedule,ProfessorConflicts)


test = linkedlist()
test.insert(3)
test.insert(4)
test.insert(5)
#print ('Head:',test.head.data)
#print ('next:',test.head.nextnode.data)
#Testing linked list class
