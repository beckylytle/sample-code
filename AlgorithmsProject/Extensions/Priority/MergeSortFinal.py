import Queue as queue

def mergesort(size, Dictionary):
    s = size
    d = Dictionary

    qq = createqueueofqueues(s)
    while type(qq) != type([1]):
        qq = rowop(qq,d)

    qq=qq[0]
    finallist = []

    while qq.empty() == False:
        finallist += [qq.get()]
    return(finallist)

def createqueueofqueues(size):
    FirstQueue = queue.Queue()
    i=0
    while i < int(size):
        i += 1
        qi = createsinglequeue(i)
        FirstQueue.put(qi)

    return(FirstQueue)

def createsinglequeue(i):
    qi = queue.Queue()
    qi.put(i)
    return(qi)

def rowop(queueofqueues, Dictionary):
    d = Dictionary
    q4 = queue.Queue()
    a = queueofqueues.get()
    try:
        b = queueofqueues.get_nowait()
    except queue.Empty:
        b = None
    if b == None:
        return([a])
    else:
        q3 = combinequeue(a,b,d)
        q4.put(q3)
        while queueofqueues.empty() == False:
            a = queueofqueues.get()
            try:
                b = queueofqueues.get_nowait()
            except queue.Empty:
                b = None
            if b == None:
                q4.put(a)
            else:
                q3 = combinequeue(a,b,d)
                q4.put(q3)
        return(q4)


def combinequeue(q1,q2,Dictionary):
    d = Dictionary
    #print d
    q3= queue.Queue()
    x = q1.get()
    y = q2.get()
    #print (x,y)
    while 1<2:
        if (d[x]>d[y]):
            q3.put(x)
            if q1.empty():
                q3.put(y)
                break
            else:
                x = q1.get()
        else:
            q3.put(y)
            if q2.empty():
                q3.put(x)
                break
            else:
                y = q2.get()


    while ((q1.empty() == True) and (q2.empty() == False))== True:
        j = q2.get()
        q3.put(j)
    while ((q1.empty() == False) and (q2.empty() == True)) == True:
        k = q1.get()
        q3.put(k)
    return(q3)

#d = {1:20,2:16,3:30,4:40,5:30,6:12,7:78,8:42,9:47,10:27,11:62,12:3,13:8,14:26,15:25}

#s = mergesort(15,d)

#print(s)
