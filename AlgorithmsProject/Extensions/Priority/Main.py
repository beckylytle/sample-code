#!/usr/bin/python

import sys
from ExtensionPriority import *
from linkedlist import *
from SetUp import *
from dataorg import *
import Queue
import MergeSortFinal
import random

def Main(class_info, studentpref, schedule):
#	os.rename(class_info, "class_info.csv")
#	os.rename(studentpref, "student_prefs.csv")
	"""Get info from txt files"""
	ClassProf, Rooms, numtimeslot, Classes, NumRooms, NumClasses = get_data_constraints(class_info)
	Timeslot = [x for x in range(1, numtimeslot+1)]
	StudentPref = get_data_studentpref(studentpref)
	"""Use this info to make data structures"""
	ClassEnrollInfo,ClassEnrollSize = MakeClassEnrollInfo(StudentPref,Classes)
	ClassRoom = MakeClassRoom(Classes)
	SortedRoom = MergeSortFinal.mergesort(NumRooms,Rooms)
	StudentsClasses = MakeStudentsClasses(StudentPref)
	#print ('StudentsClasses',StudentsClasses)
	StudentsTimeSlot = MakeStudentsTimeSlot(StudentPref,Timeslot)
	#print ('StudentsTimeSlot',StudentsTimeSlot)
	ClassPriority = MergeSortFinal.mergesort(NumClasses,ClassEnrollSize)
	Schedule = MakeSchedule(Timeslot)
	Teachers = list(set(ClassProf.values())) #Should explain time complexity later
	ProfessorConflicts = MakeProfessorConflicts(Teachers)
	ClassConflict = Queue.Queue()
	"""CREATE SENIORITY"""
	Seniority = {}
	for person in StudentsClasses.keys():
		Seniority[person] = StudentsClasses.keys().index(person)%4 + 1
	"""Pass data structures to function"""
	ClassRoom,ClassProf,ClassTimeSlot,ClassStudents = CreateSchedule(Rooms,Timeslot,ClassProf,ClassConflict,ClassEnrollInfo,ClassRoom,SortedRoom,StudentsClasses,StudentsTimeSlot,ClassPriority,Schedule,ProfessorConflicts,Seniority)
	#print ('ClassRoom',ClassRoom)
	#print('ClassProf',ClassProf)
	#print('ClassTimeSlot',ClassTimeSlot)
	#print('ClassStudents',ClassStudents)
	"""Write schedule out to schedule.txt"""
	heading= "Course" + "\t" + "Room" + "\t" + "Teacher" +"\t" + "Time" + "\t" + "Students" + "\n"
	 # content= [[1, 1, 2, 2, [2, 3, 4, 7, 8, 9, 10]],[[2,3, 2,4, [4, 9]]]

	content = []
	for item in ClassPriority:
		subcontent = []
		subcontent += [item]
		subcontent += [ClassRoom[item]]
		subcontent += [ClassProf[item]]
		subcontent += [ClassTimeSlot[item]]
		subcontent += [ClassStudents[item]] #PROBLEM: THIS IS A LINKED LIST
		content += [subcontent]

	#print content

	text_file = open(schedule, "w")
	text_file.write(heading)

	for line in content:
		#1       1       2       2       2 3 4 7 8 9 10
		testing = line[4].linkedlist2string()
		#print "HELLLLLOOOOO", testing
		text_file.write(str(line[0])+ "	" + str(line[1]) + "	" + str(line[2]) + "	" + str(line[3]) + "	" + testing + "\n")

	text_file.close()


#print "1", sys.argv[1]
#print "2", sys.argv[2]
#print "3", sys.argv[3]

class_info = str(sys.argv[1])
studentpref = str(sys.argv[2])
schedule = str(sys.argv[3])



if len(sys.argv) != 4:
  print "Usage: " + sys.argv[0] + " <enrollment.csv> <student_prefs.txt> <constraints.txt>"

  exit(1)


Main(class_info, studentpref, schedule)
