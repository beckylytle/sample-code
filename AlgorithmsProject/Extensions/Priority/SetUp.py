from linkedlist import *
import Queue
import MergeSortFinal
"""
EXAMPLE DATA -- must be able to create these using txt files later!!!!!

classes = [1,2,3,4,5,6]
timeslot = [1,2]
teachers = {1:[1,2],2:[3,4],3:[5,6]}
rooms: {1:10,2:7,3:5}
studentpref = {
1:[1,2,3,4],
2:[1,3,4,5],
3:[1,4,5,6],
4:[1,5,6,2],
5:[1,6,2,5],
6:[1,3,4,2],
7:[1,6,5,3],
8:[1,2,3,4],
9:[1,5,6,2],
10:[1,2,4,5]
}

"""


"""DATA STRUCTURES """

#NEW!!!!!!! class/prof dictionary {class:professor}
"""ClassProf = {1:1,2:1,3:2,4:2,5:3,6:3}"""

#not needed
def MakeClassProf(teachers):
     ClassProf = {}
     for x in teachers:
          firstclass = teachers[x][0]
          secondclass = teachers[x][1]
          ClassProf[firstclass] = x
          ClassProf[secondclass] = x
     return ClassProf

#create ClassEnrollInfo = { Class1 : [len(students),[list of students who want that class]] } by sorting through pref lists
# i think the list of students should be a linked list... the whole list doesnt need to be though - maddy
"""
ClassEnrollInfo = {
1: [10, [1,2,3,4,5,6,7,8,9,10]],
2: [7, [1,4,5,6,8,9,10]],
3: [5, [1,2,6,7,8]],
4: [6, [1,2,3,6,8,10]],
5: [7, [2,3,4,5,7,9,10]],
6: [5, [3,4,5,7,9]]
}

ClassEnrollSize =
{1:10,
2:7,
3:5,
}
"""
#ALSO IS NOT THE EXACT SET UP AS ABOVE ....
def MakeClassEnrollInfo(studentpref,classes):
    ClassEnrollInfo = {}
    ClassEnrollSize = {}
    for y in range(0,len(classes)):
        ClassEnrollInfo[classes[y]] = linkedlist()
    for x in studentpref:
        cls = studentpref[x]
        # we assume that there are 4 for each student
        ClassEnrollInfo[cls[0]].insert(x)
        ClassEnrollInfo[cls[1]].insert(x)
        ClassEnrollInfo[cls[2]].insert(x)
        ClassEnrollInfo[cls[3]].insert(x)
# print ClassEnrollInfo
    for item in ClassEnrollInfo:
        ClassEnrollSize[item] = ClassEnrollInfo[item].size
	#print ClassEnrollInfo[3].head.getdata()
	#print ClassEnrollInfo[3].head.getnext().getdata()
	#print ClassEnrollInfo[3].head.getnext().getnext().getdata()
	#print ClassEnrollInfo[3].head.getnext().getnext().getnext().getdata()
	#print ClassEnrollInfo[3].head.getnext().getnext().getnext().getdata()
	#print ClassEnrollInfo[3].head.getnext().getnext().getnext().getnext().getdata()
    return ClassEnrollInfo,ClassEnrollSize






#create ClassRoom = { Class1 : room assigned } initializing all values to none"
"""ClassRoom = {1:None, 2:None, 3:None, 4:None, 5:None, 6:None}"""

def MakeClassRoom(classes):
     ClassRoom = {}
     for x in classes:
          ClassRoom[x] = None
     return ClassRoom



#create SortedRoom which is list of rooms sorted by size from biggest to smallest



#use merge sort





#create StudentsClasses = { st1: [[classpreflist], [finalenrollment]]}  w/ final enrollment as [None,None,None,None]
"""
StudentsClasses = {
1:[[1,2,3,4],[None,None,None,None]],
2:[[1,3,4,5],[None,None,None,None]],
3:[[1,4,5,6],[None,None,None,None]],
4:[[1,5,6,2],[None,None,None,None]],
5:[[1,6,2,5],[None,None,None,None]],
6:[[1,3,4,2],[None,None,None,None]],
7:[[1,6,5,3],[None,None,None,None]],
8:[[1,2,3,4],[None,None,None,None]],
9:[[1,5,6,2],[None,None,None,None]],
10:[[1,2,4,5],[None,None,None,None]]
}
"""
def MakeStudentsClasses(studentpref):
     StudentClasses = studentpref
     for x in StudentClasses:
          y = StudentClasses[x]
#          StudentClasses[x] = [y, [None,None,None,None]]
          StudentClasses[x] = [y, linkedlist()]
     return StudentClasses



#create StudentsTimeSlot = { st1: [list of classes curently taking classes in]} and this list will be initialized as
#[None,None ... , None] for each possible time slot
#--> use StudentsTimeSlot[st1][0] to check if first timeslot is open, etc
"""
StudentsTimeSlot = {
1:[None,None], #2 time slots possible
2:[None,None], #timeslot correlates to index in this list , so it is easy to find
3:[None,None],
4:[None,None],
5:[None,None],
6:[None,None],
7:[None,None],
8:[None,None],
9:[None,None],
10:[None,None]
}
"""
def MakeStudentsTimeSlot(studentpref,timeslot):
     StudentsTimeSlot = {}
     NumSlot = [ None for x in timeslot]
     for student in studentpref:
          StudentsTimeSlot[student] = [ None for x in timeslot]
     return StudentsTimeSlot


#created  list ClassPriority w classes sorted most popular to less popular
"""this can just be a list created with merge sort"""

ClassPriority = [
1,2,5,4,3,6 #most to least popular
]


# create Schedule = {ts1 : [classes occurring then]} w/ empty linked list
"""IDK how to make linked list lol"""
"""
Schedule = {
1: [],
2: [],
}
"""
#not tested ;^)
def MakeSchedule(timeslot):
	Schedule = {}
	for x in timeslot:
		Schedule[x] = linkedlist()
	return Schedule # if you ask to print u would get something like this
#{1: <__main__.linkedlist object at 0x1051f3510>, 2: <__main__.linkedlist object at 0x1051f3550>}



#create ProfessorConflicts = { p1 : timeslot } where timeslot is initially None
"""
ProfessorConflicts = {
1:None,
2:None,
3:None
}
"""

def MakeProfessorConflicts(teachers):
     ProfessorConflicts = {}
     for x in teachers:
          ProfessorConflicts[x] = None
     return ProfessorConflicts


#create ClassConflict which is a queue, let it be empty

ClassConflict = Queue.Queue()

"""
Class Times	4
Rooms	4
1	84
2	89
3	18
4	59
Classes	14
Teachers	7
1	5
2	2
3	6
4	6
5	3
6	2
7	7
8	4
9	4
10	3
11	5
12	1
13	1
14	7

  Students	50
1	9 14 11 10
2	13 1 5 2
3	5 6 14 4
4	11 3 4 9
5	9 13 7 14
6	3 9 8 2
7	7 1 8 10
8	13 9 12 3
9	9 5 10 2
10	5 2 3 13
11	2 9 13 10
12	3 13 8 14
13	2 1 9 14
14	14 12 11 13
15	9 1 4 14
16	6 13 1 12
17	3 8 4 6
18	5 6 3 2
19	12 11 5 14
20	14 1 9 5
21	2 9 5 8
22	3 10 5 8
23	6 14 8 11
24	9 14 5 12
25	12 1 8 10
26	8 13 1 3
27	7 14 12 10
28	7 5 1 9
29	9 4 14 11
30	13 1 11 12
31	12 4 1 9
32	6 12 14 8
33	12 11 14 3
34	3 9 8 5
35	14 5 4 11
36	11 4 14 2
37	1 12 5 9
38	4 7 2 9
39	6 9 13 4
40	10 7 4 8
41	11 13 5 3
42	14 3 6 12
43	6 2 4 8
44	7 2 5 4
45	3 14 1 6
46	5 13 4 14
47	6 5 2 14
48	6 9 11 12
49	6 4 14 13
50	12 4 2 11
"""
