# coding: utf-8
import Queue
from linkedlist import *
from SetUp import *


"""
don't add lab classes to ClassPriority, just store in following data structures
also, make sure ClassProf has info about these classes!!!

LabClass = [1,3,5] - these are all lecture classes w associated labs
Reserved = [5,8,9] - all reserved rooms
LectureLab = {1:a,3:b,5:c} aka values are lab classes of the key
LabRoom = {a:4,b:8,c:10} lab classes with associated room
"""

def CreateSchedule(rooms, timeslot,ClassProf,ClassConflict,ClassEnrollInfo,ClassRoom,SortedRoom,StudentsClasses,StudentsTimeSlot,ClassPriority,Schedule,ProfessorConflicts,LabClass,Reserved,LectureLab,LabRoom):
    BadClass = []
    timeroom = {}
    for m in range(0,len(timeslot)):
        for k in range(0,len(SortedRoom)):
            timeroom[(m,k)] = False
    k=0 #the number of columns (aka which classroom to use)
    m=0 #the number of rows (aka which timeslot to use)
    countdown = False #this lets us know what to do with m
    n = len(ClassPriority) #n is the length of classes
    i = 0 #this lets us loop through ClassPriority
    ClassTimeSlot = {}
    for e in range(0,len(ClassPriority)):
        ClassTimeSlot[ClassPriority[e]] = None
        """NEW DATA STRUCTURE"""
    #let ClassProf[i] be the professor teaching a certain class i
    ClassStudents = {}
    for q in range(0,len(ClassPriority)):
        ClassStudents[ClassPriority[q]] = linkedlist()
        #set all values to empty lists to assign students at the end
    while i < n:
        #print (i,m,k,countdown)
        #print ('Current Class: ',ClassPriority[i])
        #print ('Current m:',m)
        #print ('Current k:',k)
        if ClassRoom[ClassPriority[i]] == None: #if class hasn't already been assigned
            if ProfessorConflicts[ClassProf[ClassPriority[i]]] == None: #Aka if professor has no previous assignments
                ProfessorConflicts[ClassProf[ClassPriority[i]]] = timeslot[m] #Set professor assignment to class i
                Schedule[timeslot[m]].insert(ClassPriority[i]) #add this class to the schedule at this time
                ClassRoom[ClassPriority[i]] = SortedRoom[k]
                ClassTimeSlot[ClassPriority[i]] = timeslot[m] #ADD THIS TIME SLOT TO THIS CLASS
                timeslotFill = True
                timeroom[(m,k)] = True
                if ClassPriority[i] in LabClass: #this class has a lab section
                    labclass = LectureLab[ClassPriority[i]]
                    room = LabRoom[labclass]
                    currentTime = 0
                    Assigned = False
                    Good = False
                    #print labclass
                    while Assigned == False:
                        #print labclass
                        if timeroom[(currentTime, room)] == False and timeroom[(currentTime+1, room)] == False and timeroom[(currentTime+2, room)] == False and ( ProfessorConflicts[ClassProf[labclass]] == None or (not ProfessorConflicts[ClassProf[labclass]] == currentTime and not ProfessorConflicts[ClassProf[labclass]] == currentTime+1 and not ProfessorConflicts[ClassProf[labclass]] == currentTime+2)):
                            ClassRoom[labclass] = room #SET ROOM
                            ClassTimeSlot[labclass] = [currentTime,currentTime+1,currentTime+2] #SET TIMESLOT
                            timeroom[(currentTime,room)] = True #SET TRUE
                            timeroom[(currentTime+1,room)] = True
                            timeroom[(currentTime+2,room)] = True
                            Schedule[timeslot[m]].insert(labclass) #SET SCHEDULE
                            ProfessorConflicts[ClassProf[labclass]] = timeslot[m] #???? unsure if this is correct
                            Assigned=True
                            Good=True
                            #print labclass
                        else:
                            currentTime += 1
                            if currentTime >= len(timeslot)-2: #aka if lab couldnt b scheduled
                                Assigned=True
                                Good=False
                                print (labclass,'BAD')
                    if Good==False: #couldn’t schedule lab
                        print 'BADCLASS'
                    	BadClass += [labclass,ClassPriority[i]] #at the end, make sure u ignore BadClass
            elif ProfessorConflicts[ClassProf[ClassPriority[i]]] == timeslot[m]: #Aka if professor is already in this time slot!
                ClassConflict.put(ClassPriority[i]) #add ClassPriority[i] to queue ClassConflict
                #print ('Putting ',ClassPriority[i],' on Queue')
                #print ClassPriority[i] #see what gets added
                timeslotFill = False
            else:
                Schedule[timeslot[m]].insert(ClassPriority[i]) #add this class to the schedule at this time
                ClassRoom[ClassPriority[i]] = SortedRoom[k]
                ClassTimeSlot[ClassPriority[i]] = timeslot[m] #ADD THIS TIME SLOT TO THIS CLASS
                timeslotFill = True
                timeroom[(m,k)] = True

                if ClassPriority[i] in LabClass: #this class has a lab section
                    labclass = LectureLab[ClassPriority[i]]
                    room = LabRoom[labclass]
                    currentTime = 0
                    Assigned = False
                    Good = False
                    #print labclass
                    while Assigned == False:
                        #print labclass
                        if timeroom[(currentTime, room)] == False and timeroom[(currentTime+1, room)] == False and timeroom[(currentTime+2, room)] == False and ( ProfessorConflicts[ClassProf[labclass]] == None or (not ProfessorConflicts[ClassProf[labclass]] == currentTime and not ProfessorConflicts[ClassProf[labclass]] == currentTime+1 and not ProfessorConflicts[ClassProf[labclass]] == currentTime+2) ):
                            ClassRoom[labclass] = room #SET ROOM
                            ClassTimeSlot[labclass] = [currentTime,currentTime+1,currentTime+2] #SET TIMESLOT
                            timeroom[(currentTime,room)] = True #SET TRUE
                            timeroom[(currentTime+1,room)] = True
                            timeroom[(currentTime+2,room)] = True
                            Schedule[timeslot[m]].insert(labclass) #SET SCHEDULE
                            ProfessorConflicts[ClassProf[labclass]] = timeslot[m] #???? unsure if this is correct
                            Assigned=True
                            Good=True
                            #print labclass
                        else:
                            currentTime += 1
                            if currentTime >= len(timeslot)-2: #aka if lab couldnt b scheduled
                                Assigned=True
                                Good=False
                                print (labclass,'BAD')
                    if Good==False: #couldn’t schedule lab
                        print 'BADCLASS'
                    	BadClass += [labclass,ClassPriority[i]] #at the end, make sure u ignore BadClass
            j = i
            """Run the following loop until current time slot is filled"""
            while timeslotFill == False and j+1<len(ClassPriority): #Run this loop until this specific time slot is filled!!!
            #don't continue this loop if you're at the end of the class list.. Just let this timeslot be empty
                if ProfessorConflicts[ClassProf[ClassPriority[j+1]]] == timeslot[m]: #check if class j+1 works
                        ClassConflict.put(ClassPriority[j+1]) #put class j+1 on queue
                        j=j+1 #increment j so we can try next class
                else:
                    #print ("Got here for",ClassPriority[j+1])
                    #print ('Filling timeslot ',timeslot[m],' with ',ClassPriority[j+1])
                    Schedule[timeslot[m]].insert(ClassPriority[j+1]) #add this class to schedule at this time
                    ProfessorConflicts[ClassProf[ClassPriority[j+1]]] = timeslot[m] #assign professor to this timeslot
                    ClassRoom[ClassPriority[j+1]] = SortedRoom[k] #assign room k to this class
                    ClassTimeSlot[ClassPriority[j+1]] = timeslot[m] #ADD THIS TIME SLOT TO THIS CLASS
                    timeslotFill = True
                    timeroom[(m,k)] = True
                    if ClassPriority[i] in LabClass: #this class has a lab section
                        labclass = LectureLab[ClassPriority[i]]
                        room = LabRoom[labclass]
                        currentTime = 0
                        Assigned = False
                        Good = False
                        #print labclass
                        while Assigned == False:
                            #print labclass
                            if timeroom[(currentTime, room)] == False and timeroom[(currentTime+1, room)] == False and timeroom[(currentTime+2, room)] == False and ( ProfessorConflicts[ClassProf[labclass]] == None or (not ProfessorConflicts[ClassProf[labclass]] == currentTime and not ProfessorConflicts[ClassProf[labclass]] == currentTime+1 and not ProfessorConflicts[ClassProf[labclass]] == currentTime+2) ):
                                ClassRoom[labclass] = room #SET ROOM
                                ClassTimeSlot[labclass] = [currentTime,currentTime+1,currentTime+2] #SET TIMESLOT
                                timeroom[(currentTime,room)] = True #SET TRUE
                                timeroom[(currentTime+1,room)] = True
                                timeroom[(currentTime+2,room)] = True
                                Schedule[timeslot[m]].insert(labclass) #SET SCHEDULE
                                ProfessorConflicts[ClassProf[labclass]] = timeslot[m] #???? unsure if this is correct
                                Assigned=True
                                Good=True
                                #print labclass
                            else:
                                currentTime += 1
                                if currentTime >= len(timeslot)-2: #aka if lab couldnt b scheduled
                                    Assigned=True
                                    Good=False
                                    print (labclass,'BAD')
                        if Good==False: #couldn’t schedule lab
                            print 'BADCLASS'
                            BadClass += [labclass,ClassPriority[i]] #at the end, make sure u ignore BadClass
            #print Schedule
            """Now, run through the Queue and place these Classes in time slots"""
            if ClassConflict.empty() == False:
                while ClassConflict.empty() == False:
                    #print ('m',m)
                    #print ('k',k)
                    #while queue is not empty
                    #PROBLEM: this enters infinite loop if current time slot works for nothing on the queue!
                    #HOW TO FIX: check if current time slot has already been tried by everything on queue!
                    q = ClassConflict.get() #q is the class popped off the queue
                    #print q
                    origM = m
                    origK = k
                    origCD = countdown #save all current values
                    #print m
                    while timeroom[(m,k)] == True:
                        if m==len(timeslot)-1 and countdown == False: #aka if m is the last timeslot in the list of timeslots
                            countdown = True #let countdown be True
                            k=k+1
                            while k in Reserved:
                                k=k+1
                            m=m+1 #increment both k and m
                        elif m==0 and countdown == True: #aka if m is at the first time slot in the list of timeslots & it's been counting down
                            countdown = False #switch value of countdown
                            k = k +1
                            while k in Reserved:
                                k=k+1
                            m=m-1 #increment k, subtract 1 from m
                        if countdown == True: #if we're counting down rn
                            #print m
                            m=m-1
                            #print 'decrement m'
                            #print m
                        else:
                            m=m+1
                            #print 'increment m'
                    """ The above stuff is just a way to increment the time slot and classroom in the way we discussed
                    This is necessary because the original timeslot did not work for these & that's why they're on the Queue"""
                    if not (ProfessorConflicts[ClassProf[q]] == timeslot[m]): #if this timeslot works for q
                        Schedule[timeslot[m]].insert(q) #add this class to schedule at this time
                        ProfessorConflicts[ClassProf[q]] = timeslot[m] #assign professor to this timeslot
                        ClassRoom[q] = SortedRoom[k] #assign room k to this class
                        ClassTimeSlot[q] = timeslot[m] #ADD THIS TIME SLOT TO THIS CLASS
                        timeroom[(m,k)] = True
                        #print ('Putting class ',q," in timeslot ",timeslot[m])
                        #print ('Assigning',q,"from queue")
                        if ClassPriority[i] in LabClass: #this class has a lab section
                            labclass = LectureLab[ClassPriority[i]]
                            room = LabRoom[labclass]
                            currentTime = 0
                            Assigned = False
                            Good = False
                            #print labclass
                            while Assigned == False:
                                #print labclass
                                if timeroom[(currentTime, room)] == False and timeroom[(currentTime+1, room)] == False and timeroom[(currentTime+2, room)] == False and ( ProfessorConflicts[ClassProf[labclass]] == None or (not ProfessorConflicts[ClassProf[labclass]] == currentTime and not ProfessorConflicts[ClassProf[labclass]] == currentTime+1 and not ProfessorConflicts[ClassProf[labclass]] == currentTime+2) ):
                                    ClassRoom[labclass] = room #SET ROOM
                                    ClassTimeSlot[labclass] = [currentTime,currentTime+1,currentTime+2] #SET TIMESLOT
                                    timeroom[(currentTime,room)] = True #SET TRUE
                                    timeroom[(currentTime+1,room)] = True
                                    timeroom[(currentTime+2,room)] = True
                                    Schedule[timeslot[m]].insert(labclass) #SET SCHEDULE
                                    ProfessorConflicts[ClassProf[labclass]] = timeslot[m] #???? unsure if this is correct
                                    Assigned=True
                                    Good=True
                                    #print labclass
                                else:
                                    currentTime += 1
                                    if currentTime >= len(timeslot)-2: #aka if lab couldnt b scheduled
                                        Assigned=True
                                        Good=False
                                        print (labclass,'BAD')
                            if Good==False: #couldn’t schedule lab
                                print 'BADCLASS'
                            	BadClass += [labclass,ClassPriority[i]] #at the end, make sure u ignore BadClass
                    else:
                        if ClassConflict.empty() == False:
                            m = origM
                            k = origK
                            Countdown = origCD #Reset all these values
                            ClassConflict.put(q) #add q to ClassConflict again
                        else:
                            ClassConflict.put(q)
                            #print m
                        #now, we'll try the next item in the queue on this
            elif ClassConflict.empty() == True:
                while timeroom[(m,k)] == True:
                    if m==len(timeslot)-1 and countdown == False: #aka if m is the last timeslot in the list of timeslots
                        countdown = True #let countdown be True
                        k=k+1
                        while k in Reserved:
                            k=k+1
                        m=m+1 #increment both k and m
                        #print "CHECKPOINT1"
                    elif m==0 and countdown == True: #aka if m is at the first time slot in the list of timeslots & it's been counting down
                        countdown = False #switch value of countdown
                        k=k+1
                        while k in Reserved:
                            k=k+1
                        m=m-1
                        #increment k, subtract 1 from m
                        #print "CHECKPOINT2"
                    if countdown == True: #if we're counting down rn
                        m=m-1
                        #print "CHECKPOINT3"
                    else:
                        m=m+1
                        #print "CHECKPOINT4"
                """Again, this is just incrementing the time slot to the next one that makes sense based on
                how we wanted to assign time slots"""
        else:
            while timeroom[(m,k)] == True:
                if m==len(timeslot)-1 and countdown == False: #aka if m is the last timeslot in the list of timeslots
                    countdown = True #let countdown be True
                    k=k+1
                    while k in Reserved:
                        k=k+1
                    m=m+1 #increment both k and m
                    #print "CHECKPOINT1"
                elif m==0 and countdown == True: #aka if m is at the first time slot in the list of timeslots & it's been counting down
                    countdown = False #switch value of countdown
                    k=k+1
                    while k in Reserved:
                        k=k+1
                    m=m-1
                    #increment k, subtract 1 from m
                    #print "CHECKPOINT2"
                if countdown == True: #if we're counting down rn
                    m=m-1
                    #print "CHECKPOINT3"
                else:
                    m=m+1
                    #print "CHECKPOINT4"
        i = i + 1 #increment i

    #print ClassRoom
    #print ('StudentsTimeSlot',StudentsTimeSlot)
    #For each class j in ClassPriority (starting with least popular class):  """!!!!!!!!"""
    experimentalCounter = 0 #THIS WILL COUNT the amount of preferences HONORED!!
    z = len(ClassPriority) - 1 #Start with last element & go down
    while z > -1:
        if ClassPriority[z] in LabClass:
            if LectureLab[ClassPriority[z]] in ClassRoom.keys():
                lab = True
            else:
                lab = False
        else:
            lab = True
        if not ClassPriority[z] in BadClass and lab == True:

            #print ('Current Class',ClassPriority[z])
            if not ClassPriority[z] in LabClass:
                Size = rooms[ClassRoom[ClassPriority[z]]] #get size of classroom that this class is in!
            else:
                labclass = LectureLab[ClassPriority[z]]
                #print (ClassPriority[z],labclass)
                Size = min(rooms[ClassRoom[ClassPriority[z]]],rooms[ClassRoom[labclass]])
                #choose the smaller out of the two
            #print Size
            a=0 #a represents going through size of classroom
            b=0 #b represents the specific student
            OriginalHead = ClassEnrollInfo[ClassPriority[z]].head
            #print ('ADDING STUDENTS TO CLASS', ClassPriority[z])
            #print ('SIZE OF CLASS',Size)
            #print ('STUDENTS THAT WANT THIS CLASS',ClassEnrollInfo[ClassPriority[z]].size)
            while a <= Size: #running through possible students in class
                    # StudentsClasses[student] holds class pref list and final enrollment
                    #print ('a',a,'b',b)
                    if not ClassPriority[z] in LabClass:
                        #currentstudent = ClassEnrollInfo[ClassPriority[z]][1][b] the bth student in list of students who want this class
                        currentstudent = ClassEnrollInfo[ClassPriority[z]].head
                        #print ('CURRENT STUDENT:',currentstudent)
                        currenttimeslot = ClassTimeSlot[ClassPriority[z]] #timeslot of class z
                        #print ('timeslot',currenttimeslot)
                        #print ('student',currentstudent.data)
                        #print StudentsTimeSlot
                        #print ('???',StudentsTimeSlot[currentstudent.data][currenttimeslot-1])
                        if StudentsTimeSlot[currentstudent.data][currenttimeslot-1] == None: #if student doesn't have a conflict at time of class z
                            StudentsClasses[currentstudent.data][1].insert([ClassPriority[z]])
                            experimentalCounter += 1
                            #StudentsClasses[currentstudent.data][1] += [ClassPriority[z]] #add class z to enrollment list of student b
                            StudentsTimeSlot[currentstudent.data][currenttimeslot-1] = ClassPriority[z] #assign class to student's time slot
                            ClassStudents[ClassPriority[z]].insert(currentstudent.data)
                            #print 'Added current student to ClassStudents'
                            """ADD TO CLASS SCHEDULE"""
                            a = a+1
                            #increment a so we know a seat in the room has been taken
                    else:
                        #currentstudent = ClassEnrollInfo[ClassPriority[z]][1][b] the bth student in list of students who want this class
                        currentstudent = ClassEnrollInfo[ClassPriority[z]].head
                        #print ('CURRENT STUDENT:',currentstudent)
                        labclass = LectureLab[ClassPriority[z]]
                        currenttimeslot = ClassTimeSlot[ClassPriority[z]] #timeslot of class z
                        currenttimeslot2 = ClassTimeSlot[labclass][0]
                        currenttimeslot3 = ClassTimeSlot[labclass][1]
                        currenttimeslot4 = ClassTimeSlot[labclass][2]
                        #print ('timeslot',currenttimeslot)
                        #print ('student',currentstudent.data)
                        #print StudentsTimeSlot
                        #print ('???',StudentsTimeSlot[currentstudent.data][currenttimeslot-1])
                        if StudentsTimeSlot[currentstudent.data][currenttimeslot-1] == None and StudentsTimeSlot[currentstudent.data][currenttimeslot2-1] == None and StudentsTimeSlot[currentstudent.data][currenttimeslot3-1] == None and StudentsTimeSlot[currentstudent.data][currenttimeslot4-1] == None: #if student doesn't have a conflict at time of class z
                            StudentsClasses[currentstudent.data][1].insert([ClassPriority[z]])
                            experimentalCounter += 1
                            #StudentsClasses[currentstudent.data][1] += [ClassPriority[z]] #add class z to enrollment list of student b
                            StudentsTimeSlot[currentstudent.data][currenttimeslot-1] = ClassPriority[z] #assign class to student's time slot
                            StudentsTimeSlot[currentstudent.data][currenttimeslot2-1] = labclass
                            StudentsTimeSlot[currentstudent.data][currenttimeslot3-1] = labclass
                            StudentsTimeSlot[currentstudent.data][currenttimeslot4-1] = labclass
                            ClassStudents[ClassPriority[z]].insert(currentstudent.data)
                            #print 'Added current student to ClassStudents'
                            """ADD TO CLASS SCHEDULE"""
                            a = a+1
                            #increment a so we know a seat in the room has been taken
                    b=b+1 #move through student list no matter what
                    ClassEnrollInfo[ClassPriority[z]].head = currentstudent.nextnode #Set new head to current head's next node!
                    #if b is now bigger than the length of students who wanted this class, exit loop!!
                    #if b >= len(ClassEnrollInfo[ClassPriority[z]][1]):
                    if b >= ClassEnrollInfo[ClassPriority[z]].size:
                        a = Size + 1 #exit loop by breaking loop condition
            ClassEnrollInfo[ClassPriority[z]].head = OriginalHead
            z = z - 1
        else:
            z=z-1
    #return (StudentsClasses,Schedule)
    #print ClassStudents
    #print StudentsClasses
    print ('HONORED',experimentalCounter)
    print ('TOTAL',4*len(StudentsClasses.keys()))

    return (ClassRoom,ClassProf,ClassTimeSlot,ClassStudents) #these dictionaries have all info
    """ this return statement will b replaced w writing to a txt file!!"""
