#!/usr/bin/python

import sys
from ExtensionLabOLD import *
from linkedlist import *
from SetUp import *
from dataorg import *
import Queue
import MergeSortFinal
import random
"""
also, make sure ClassProf has info about these classes!!!

LabClass = [1,3,5] - these are all lecture classes w associated labs
Reserved = [5,8,9] - all reserved rooms
LectureLab = {1:a,3:b,5:c} aka values are lab classes of the key
LabRoom = {a:4,b:8,c:10} lab classes with associated room
"""
def CreateLabData(ClassProf,Classes,Rooms):
	LabClass = []
	LectureLab = {}
	amtClasses = len(Classes)
	Reserved = []
	LabRoom = {}
	LabClass += Classes[0:len(Classes)/5] #get a fifth of the classes
	amtRooms = len(LabClass)/2
	for i in range(0,amtRooms):
		Reserved += [Rooms.keys()[i]] #add 1/2 amt of reserved rooms
	for j in range(0,len(LabClass)):
		amtClasses += 1
		LectureLab[LabClass[j]] = amtClasses
		LabRoom[amtClasses] = random.choice(Reserved)
		ClassProf[amtClasses] = ClassProf[LabClass[j]] #new values for classprof
	#print LectureLab
	#print LabClass
	return (ClassProf,LabClass,Reserved,LectureLab,LabRoom)

def Main(class_info, studentpref, schedule):
#	os.rename(class_info, "class_info.csv")
#	os.rename(studentpref, "student_prefs.csv")
	"""Get info from txt files"""
	ClassProf, Rooms, numtimeslot, Classes, NumRooms, NumClasses = get_data_constraints(class_info)
	Timeslot = [x for x in range(1, numtimeslot+1)]
	StudentPref = get_data_studentpref(studentpref)
	"""Use this info to make data structures"""
	ClassEnrollInfo,ClassEnrollSize = MakeClassEnrollInfo(StudentPref,Classes)
	ClassRoom = MakeClassRoom(Classes)
	SortedRoom = MergeSortFinal.mergesort(NumRooms,Rooms)
	StudentsClasses = MakeStudentsClasses(StudentPref)
	#print ('StudentsClasses',StudentsClasses)
	StudentsTimeSlot = MakeStudentsTimeSlot(StudentPref,Timeslot)
	#print ('StudentsTimeSlot',StudentsTimeSlot)
	ClassPriority = MergeSortFinal.mergesort(NumClasses,ClassEnrollSize)
	Schedule = MakeSchedule(Timeslot)
	Teachers = list(set(ClassProf.values())) #Should explain time complexity later
	ProfessorConflicts = MakeProfessorConflicts(Teachers)
	ClassConflict = Queue.Queue()
	"""Pass data structures to function"""
	ClassProf,LabClass,Reserved,LectureLab,LabRoom = CreateLabData(ClassProf,Classes,Rooms)
	ClassRoom,ClassProf,ClassTimeSlot,ClassStudents = CreateSchedule(Rooms,Timeslot,ClassProf,ClassConflict,ClassEnrollInfo,ClassRoom,SortedRoom,StudentsClasses,StudentsTimeSlot,ClassPriority,Schedule,ProfessorConflicts,LabClass,Reserved,LectureLab,LabRoom)
	#print ('ClassRoom',ClassRoom)
	#print('ClassProf',ClassProf)
	#print('ClassTimeSlot',ClassTimeSlot)
	#print('ClassStudents',ClassStudents)
	"""Write schedule out to schedule.txt"""
	heading= "Course" + "\t" + "Room" + "\t" + "Teacher" +"\t" + "Time" + "\t" + "Students" + "\n"
	 # content= [[1, 1, 2, 2, [2, 3, 4, 7, 8, 9, 10]],[[2,3, 2,4, [4, 9]]]

	content = []
	for item in ClassPriority:
		subcontent = []
		subcontent += [item]
		subcontent += [ClassRoom[item]]
		subcontent += [ClassProf[item]]
		subcontent += [ClassTimeSlot[item]]
		subcontent += [ClassStudents[item]] #PROBLEM: THIS IS A LINKED LIST
		content += [subcontent]

	#print content

	text_file = open(schedule, "w")
	text_file.write(heading)

	for line in content:
		#1       1       2       2       2 3 4 7 8 9 10
		testing = line[4].linkedlist2string()
		#print "HELLLLLOOOOO", testing
		text_file.write(str(line[0])+ "	" + str(line[1]) + "	" + str(line[2]) + "	" + str(line[3]) + "	" + testing + "\n")

	text_file.close()


#print "1", sys.argv[1]
#print "2", sys.argv[2]
#print "3", sys.argv[3]

class_info = str(sys.argv[1])
studentpref = str(sys.argv[2])
schedule = str(sys.argv[3])



if len(sys.argv) != 4:
  print "Usage: " + sys.argv[0] + " <enrollment.csv> <student_prefs.txt> <constraints.txt>"

  exit(1)


Main(class_info, studentpref, schedule)
