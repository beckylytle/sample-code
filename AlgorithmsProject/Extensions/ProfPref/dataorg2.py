#!/usr/bin/python
import os, sys
import csv
import sys


"""
This takes the text files Constraint.txt and Student_prefs.txt and puts them into semi-usable data structures that
will be passed to ProjectCodeSetUp340 and ProjectCode340
"""

# uncomment later ...
#os.rename("Constraints.txt","Constraints.csv")
#os.rename("Studentprefs.txt","Studentprefs.csv")


def get_data_constraints(filename):
    Timeslots = ''
    NumClasses = ''
    Teachers = {}
    Rooms = {}
    NumRooms = ''
    Classes = []
    with open(filename) as f:
        f_csv = csv.reader(f,delimiter='\t')
        section = ''
        for row in f_csv:
            items= row
            # print "items", items
            if items[0] == 'Class Times':
                Timeslots = int(items[1])
            elif items[0] == 'Rooms':
                NumRooms = int(items[1])
                section = 'Rooms'
            elif items[0] == 'Classes':
                NumClasses = int(items[1])
            elif items[0] == 'Teachers':
                NumTeachers = int(items[1])
                section = 'Teachers'
            else: #add something to one of the dictionaries
                if section == 'Teachers': # {class :prof}
                    Teachers[int(items[0])] =int(items[1])
                    Classes.append(int(items[0])) # may be wrong
                else: #sections == Rooms
                    Rooms[int(items[0])] =int(items[1])
    return (Teachers, Rooms, Timeslots, Classes, NumRooms, NumClasses)

def get_data_studentpref(filename):
  studentpref = {}
  profpref = {}
  with open(filename) as f:
    f_csv = csv.reader(f,delimiter='\t')
    section = ''
    for row in f_csv:
      items= row
   #   print "items", items
      if items[0] == 'Students':
        NumStudents = int(items[1])
	section = 'students'

      elif items[0] == 'Professors':
        section = 'professors'
      elif section == 'students': #add something to the dictionaries for students
        classlist = items[1].split()
	classlist = map(int, classlist) #changes everything to an int from string
	studentpref[int(items[0])] = classlist

      elif section == 'professors' :
	preflist = items[1].split()
        preflist = map(int, preflist) #changes everything to an int from string
	profpref[int(items[0])] = preflist

  return studentpref, profpref


#Teachers, Rooms, Timeslots, Classes = get_data_constraints("demo_constraints.csv")
#print Teachers
#print Rooms
#print Timeslots

#print get_data_studentpref("demo_studentprefs.csv")





#x, y =  get_data_studentpref("studp.txt")
#print y


"""
buzzwords that we should be able to handle

- Class Times
  simple assignment of Timeslots = 4
- Rooms
  dictionary of {room:size}
- Classes
  simple assignment Classes = 14
- Teachers
  dictionary of {prof:class}
"""
