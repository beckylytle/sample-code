"""
i'm gonna use this to actually make the graph
so, you're gonna need to:
1. read in the csv (using argparse) -- turn it into: rows? dictionaries? find this out
2. use my kdtree / query files to find k-nearest neighbors of each node in my data
3. save this info in an adjacency list
4. throw this adjacency list into the graph.py file // IMPLEMENT PAGERANK
5. write out adjacency list + all the pagerank info into a json
6. somehow do the visualization


recall, adjacency list:
{node1: [list of k nearest neighbors], node2: [same idea] ...}

"""
import json
import csv
import argparse
from KDTree import KDTree
from Fruit import Fruit #maybe don't do this, bc look @ her node class first
from Point import Point
#from graph import *
from Node import Node
from Query import *
#from clean_data import *

parser = argparse.ArgumentParser()

parser.add_argument('file1',type=argparse.FileType('r'),help="input original file",nargs=1)

args = parser.parse_args()

file1 = args.file1


"""
make kd tree w/ points given (listofpoints), use "quer" function to find nearest neighbor, add that to the
adjlist in the form of its actual point (not just a new point w its values .. im pretty sure),
then remove that point from listofpoints so it can't be found as a new nearest neighbor in the recursive call
"""


#write a function that finds the k nearest neighbors (in the form of a list) for a certain row in the data
def nearestk(adjlist,row,k,k2,listofheaders,listofpoints):
	#k starts as 1 and is incremented each recursive call; k2 stays the same and is the amount of nearest neighbors being found
	if k<k2 or k==k2:	
		treelmao = KDTree(listofpoints,0,[0,1,2,3,4,5],"None")
		tre = treelmao.treenode
		#query thru "treelmao" to find nearest neighbor
		nearest1 = quer(tre,row,[],treelmao.treenode.key,listofpoints)
		#nearest1 is in the form of a list, not a point
#		print "if something goes wrong, check the next couple lines"
		party = 0
		sickening = 0
		for i in range(0,len(listofpoints)):
			if listofpoints[i].values == nearest1:
				sickening = i
				party = listofpoints[i]
		adjlist = adjlist + [party]
		newlistofpoints = []
		for j in range(0,len(listofpoints)):
			if not j == sickening:
				newlistofpoints = newlistofpoints + [listofpoints[j]]
		k=k+1
		return nearestk(adjlist,row,k,k2,listofheaders,newlistofpoints)
	else:
		return adjlist
	

#this is outside of the function
rowz,listofpoints,listofheaders = basics()

#everytime u call "nearestk" u want to pass in a listofpoints without the "row" ur passing in, bc u dont want it to make the kd
#tree with that point
original = []
for b in range(0,len(listofpoints)):
	original = original + [listofpoints[b]]

#pass in nearestk([],ExampleDataPoint,0,5,"",listofpoints)
# so i guess ur gonna wanna do a loop!! lol ! ur fav

#set k2 equal to something
k2=5

adjdic = {}

for a in range(0,len(original)):
	modified = []
	#create list of points that doesn't include the current point
	for c in range(0,len(original)):
		if not a==c:
			modified = modified + [original[c]]
	#get adjacency list for current point
	adjlist = nearestk([],original[a],1,k2,listofheaders,modified)
	#add key:value pair to the adjacency dictionary (point: 
	adjdic[original[a]] = adjlist
	print "row#",a
#	print "length of adjlist ",len(adjlist)
#NOW, adj dictionary is in the form of POINTS !! it needs to be in the form of NODE NAMES !! but idk what this means 

print "it's done, homie"

nodelist = []
for d in range(0,len(original)):
	newnode = Node(original[d])
	yeah = original[d].values[13]
	if int(yeah) == 1:
		yeah = "Thing"
	elif int(yeah) == 2:
		yeah = "Event"
	elif int(yeah) ==3:
		yeah = "Place"
	else:
		yeah = "Person"
	newnode.name = "Point #" + str(d) + ": </b></p><p>Name: " + original[d].values[1]+ "</p><p>Date Added: " + original[d].values[4] + "</p><p>Rarity: " + original[d].values[8] + "</p><p>Type: " + yeah 
	newnode.in_nodes = []
	newnode.pagerank = (1/364)
	nodelist = nodelist + [newnode]

nodedic = {}

for e in range(0,len(original)):
	thelist = adjdic[original[e]]
	newnodelist = []
	for f in range(0,len(thelist)):
		for g in range(0,len(nodelist)):
			if thelist[f].values == nodelist[g].pointvalue.values:
				newnodelist = newnodelist + [nodelist[g]]
	nodedic[nodelist[e]] = newnodelist

#OK SO I THINK nodedic is a version of adjdic but with nodes from sorelle's node class instead
#i know it says she wants names but that feels Weird to me so im not doin it yet
# jk im gonna do it now bc i guess i have to

namedic = {}

for h in range(0,len(nodelist)):
	hehe = nodedic[nodelist[h]]
	namelist = []
	for l in range(0,len(hehe)):
		namelist = namelist + [hehe[l].name]
	namedic[nodelist[h].name] = namelist


#namedic is what i need 2 pass into sorelle's graph class i think

#SETTING EACH NODE's "node.in_nodes" list
for n in range(0,len(nodelist)):
	for o in range(0,len(nodedic[nodelist[n]])):
		name1 = nodedic[nodelist[n]][o].name
		for p in range(0,len(nodelist)):
			if nodelist[p].name == name1:
				nodelist[p].in_nodes = nodelist[p].in_nodes + [nodelist[n]]
		
for q in range(0,len(nodelist)):
	print len(nodelist[q].in_nodes)


"""
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
OKAY, now you're going to take this adjacency list and use it to calculate the PageRank for each point in the data
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Instruction (written by me):
- you should use one of the node adjacency lists, either the node or node name one
- you're going to need to easily know what nodes point in to a certain node, so calculate this ahead of time
- then you'll use this info (node.in_nodes) or nodedic to get the in/out nodes for a certain node
- pagerank function
"""

def pagerank(i,nodelist,nodedic):
	if i<100:
		holder=[]
		d = 0.85
		n = 364
		teleport = (1-d)/n	
		#compute new pageranks and save them in holder list
		for s in range(0,len(nodelist)):
			value = 0.0
			for t in range(0,len(nodelist[s].in_nodes)):
				frac = nodelist[s].in_nodes[t].pagerank
				frac2 = len(nodedic[nodelist[s].in_nodes[t]])
				value = value + ((1.0*frac)/frac2)
			newvalue = teleport + d*value
			holder = holder + [newvalue]
		#now assign new pageranks from holder list to nodelist nodes
		for u in range(0,len(nodelist)):
			nodelist[u].pagerank = holder[u]
		i=i+1
		return pagerank(i,nodelist,nodedic)
	else:
		pr = []
		for v in range(0,len(nodelist)):
			pr = pr + [nodelist[v].pagerank]	
		return nodelist,pr

newbiglist,pageranklist = pagerank(0,nodelist,nodedic)			

#pageranklist.sort()
#print pageranklist

"""
name - must be unique
group - governs the color
pr - governs the radius
source/target - where to put the link


{

"nodes": [
{"name": "unique name here", "group": 5, "pr": 0.02}
]

"links": [ 
{"source":1, "target":0, "value":1}
]

}


"""

def newpr(pr):
	if pr > 0.000412 and pr < 0.000482:
		return 3
	elif pr > 0.000482 and pr < 0.0004986:
		return 3
	elif pr < 0.0006924 and pr > 0.0004986:
		return 4
	elif pr > 0.000704 and pr < 0.001136:
		return 4
	elif pr > 0.001136 and pr < 0.00153:
		return 5
	elif pr > 0.00153 and pr < 0.002492:
		return 5
	elif pr > 0.002492 and pr < 0.004042:
		return 6	
	elif pr > 0.004042 and pr < 0.005024:
		return 7
	elif pr > 0.005024 and pr < 0.00607:
		return 8
	elif pr > 0.00607 and pr < 0.007349:
		return 8
	elif pr > 0.007349 and pr < 0.00925:
		return 9
	elif pr > 0.00925 and pr < 0.01452:
		return 9
	elif pr > 0.01452 and pr < 0.03948:
		return 10
	elif pr > 0.03948 and pr < 0.04889:
		return 11
	elif pr > 0.04889:
		return 12


jsondic = {"nodes":[],"links":[]}
for w in range(0,len(nodelist)):
	#12 = thing
	#13 = event
	#14 = place
	#15 = person
	groupnum = 0
	# nodelist[w].pointvalue.values[4] is date added
	val = nodelist[w].pointvalue.values[4]
	# 9 / 1 / 2014  -- > month = val[0:1], date: val[2:3]
	# 9 / 15 / 2014 -- > month = val[0:1], date: val[2:4]
	# 10 / 6 / 2014 -- > month = val[0:2], date: val[3:4]
	# 10 / 20 / 2014 -- > month = val[0:2], date: val[3:5]
	print val
	"""
	if '2015' in val:
		groupnum = 13
	elif int(val[0:1]) == 9:
		if "/" in val[2:4]: #aka if the date is a single digit number
			if int(val[2:3]) < 8:
				groupnum = 1 #1st syllabus week
			else:
				groupnum = 2 #2nd syllabus week
		else:
			if int(val[2:4]) < 16:
				groupnum = 2
			elif int(val[2:4]) < 23:
				groupnum = 3
			elif int(val[2:4]) < 30:
				groupnum = 4
			else:
				groupnum = 5
	elif int(val[0:2]) == 10:
		if "/" in val[3:5]: #aka if date is a single digit number
			if int(val[3:4]) < 7:
				groupnum = 5
			else:
				groupnum = 6
		else:
			if int(val[3:5]) < 21:
				groupnum = 6
			elif int(val[3:5]) < 28:
				groupnum = 7
			else:
				groupnum = 8 
	elif int(val[0:2]) == 11:
		if "/" in val[3:5]:
			if int(val[3:4]) < 4:
				groupnum = 8
			else:
				groupnum = 9
		else:
			if int(val[3:5]) < 11:
				groupnum = 9
			elif int(val[3:5]) < 18:
				groupnum = 10
			elif int(val[3:5]) < 25:
				groupnum = 11
			else:
				groupnum = 12
	else:
		if int(val[3:4]) == 1:
			groupnum = 12
		else:
			groupnum = 13		 
	"""

	groupnum = float(nodelist[w].pointvalue.values[18]) + 1
	print groupnum
#	groupnum=0	
#	print nodelist[w].pointvalue.values[11]
#	if float(nodelist[w].pointvalue.values[13]) == 1:
#		groupnum = 1
#	elif float(nodelist[w].pointvalue.values[11]) <  4:
#		groupnum = 2
#	elif float(nodelist[w].pointvalue.values[11]) < 6:
#		groupnum = 3
#	elif float(nodelist[w].pointvalue.values[11]) < 7:
#		groupnum = 4
#	else:
#		groupnum = 2
	#groupnum = 1+float(nodelist[w].pointvalue.values[9])
	#print "groupnum:",groupnum
	jsondic["nodes"] = jsondic["nodes"] + [{"name":nodelist[w].name,"group":groupnum,"pr":newpr(pageranklist[w])}]
	for z in range(0,len(nodedic[nodelist[w]])):
		#find out what node the target is
		number = nodedic[nodelist[w]][z].name.find(':')
		haha = nodedic[nodelist[w]][z].name[7:number]
		hahaha = int(haha) 
		#check if the opposite link already exists in jsondic["links"]
		maddy = 0 
		for aa in range(0,len(jsondic["links"])):
			currentdic = jsondic["links"][aa] 
			if currentdic["source"] == hahaha and currentdic["target"] == w:
				maddy = maddy + 1	
		if maddy==0:
			jsondic["links"] = jsondic["links"] + [{"source":w,"target":hahaha,"value":2}]


#jsondic["links"] = jsondic["links"] + [{"source":1,"target":2,"value":2}]

pageranklist.sort()

print pageranklist

with open('sample2.json', 'w') as fp:
	json.dump(jsondic, fp)







