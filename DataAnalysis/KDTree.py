# -*- coding: utf-8 -*-
#!/usr/bin/python

from Fruit import Fruit
from Point import Point

class KDTree:

#Create a KDTree class that can create a kd-tree when given a list of Points.
#For debugging purposes, you may want to add the functionality to print out
#the tree as well.

	def __init__(self,listoflist,depth,badnum,missingval):
		self.listoflist = listoflist
		pointlist = self.getfull(self.listoflist,missingval)
                pt,before,after = self.find(pointlist,depth)
                self.treenode = Fruit(pt)
		length = len(pt.values)
		bad = badnum	
		while depth+1 in bad:
			depth=depth+1
		if not before == [] and not after == []:
                	self.treenode.leftChild = KDTree(before,(depth+1)%length,bad,missingval)
                	self.treenode.rightChild = KDTree(after,(depth+1)%length,bad,missingval)
		elif before == [] and after == []:
#			print "tree!"
			becky=0
		elif before == []:
			self.treenode.rightChild = KDTree(after,(depth+1)%length,bad,missingval)
		else:
			self.treenode.leftChild = KDTree(before,(depth+1)%length,bad,missingval)		

# [0,1,4,5,6,8,9]

	def __repr__(self):
		if self.leftChild == None and self.rightChild == None:
		    return [self.key, [],[]]
		elif self.leftChild==None:
		    return [self.key, [],[self.rightChild]]
		elif self.rightChild==None:
		    return [self.key, [self.leftChild],[]]
		else:
		    return [repr(self.key), [repr(self.leftChild)],[repr(self.rightChild)]]

	def get_rows(filename): #listofrows
        	rows=[]
        	fh=open('jasontest.csv','rU')
        	for line in fh:
                	rows.append(line.strip().split(','))
        	return rows[1:]

	def getfull(self,lst,missingval):	
		#lst is a list of POINTS
		temp = lst
		new=[]
		for x in range(0,len(temp)):
			if not missingval in temp[x].values:
			#if not "None" in temp[x].values and not "#VALUE!" in temp[x].values:
				new=new+[temp[x]]
		return new

	def getunfull(self,lst,missingval):
		temp=lst
		new1=[]
		for y in range(0,len(temp)):
			if missingval in temp[x].values:
		#	if "None" in temp[x].values or "#VALUE!" in temp[x].values:
				new1=new1+[temp[x]]		
		return temp


#here's the median finding algorithm from median.py
        def find(self,points,dim):
                val = []
                for i in range(0,len(points)):
			val = val +[[points[i].values[dim], points[i]]]
                val.sort(key=lambda x:x[0])
		if len(val)==0:
			print "idk"
               	elif len(val)%2==0:
                        value = val[len(val)/2][0]
                        pt = val[len(val)/2][1]
                        before = []
                        for j in range(0,len(val)/2):
                                before = before + [val[j][1]]
                        after = []
                        for x in range((len(val)/2)+1,len(val)):
                                after = after + [val[x][1]]
                        return (pt,before,after)
                elif not len(val)%2==0 and not len(val) == 0:
                        value = val[(len(val)-1)/2][0]
                        pt = val[(len(val)-1)/2][1]
                        before = []
                        for l in range(0,(len(val)-1)/2):
                                before = before + [val[l][1]]
                        after = []
                	for y in range((len(val)/2)+1,len(val)):
                        	after = after + [val[y][1]]
                	return (pt,before,after)
		else:
			print "idk"




	#always pass in d=0/tree1=None at first when u call this!
#	def tree(self,d,points,tree1,side,amt):
#		#amt is gonna hold the info for which dim to ignore, which dim's not to ignore
#		#first, find median point to put as node
#		new = KDTree(points)
#		pt,before,after = new.find(points,d)
#		#make a node called "mom" that has the val of this median point
#		if not tree1 == None:
#			if side == "left":
#				tree1.leftChild = Node(pt)
#				k=0
#			else:
#				tree1.rightChild = Node(pt)
#				k=1
#		else:
#			mom = Node(pt)
#		if len(before)==0 and len(after)==0:
#			return tree1 
#		#ok, now, recursively call left and right children w/ before & after lists (?)
#		else:
#			if k==0:
#				tree1.leftChild.leftChild = tree(d+1,before,tree1.leftChild,"left",amt)
#				tree1.leftChild.rightChild= tree(d+1,after,tree1.leftChild,"right",amt)   
#			elif k==2:
#				mom.leftChild = tree(d+1,before,mom,"left",amt)
#				mom.rightChild = tree(d+1,after,mom,"right",amt)
#			else:
#				tree1.rightChild.leftChild = tree(d+1,before,tree1.rightChild,"left",amt)
#				tree1.rightChild.rightChild = tree(d+1,after,tree1,rightChild,"right",amt)


#pt1=Point([1,2,3,4,5])
#pt2 = Point([3,7,8,9,4])
#pt3 = Point([8,12,15,20,5])
#pt4 = Point([1,1,1,1,1])
#pt5 = Point([10,10,10,10,10])

#points = [pt1,pt2,pt3,pt4,pt5]

#tree(0,points,None,None,5)


